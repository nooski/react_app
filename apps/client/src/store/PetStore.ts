import { makeAutoObservable, action, observable } from 'mobx'

class PetStore {
  constructor(
    public pets: any[] = [],
    public petID: number = 0
  ) {
    makeAutoObservable(this, {
      pets: observable,
      setPets: action,
      petID: observable,
      setPetID: action
    })
    this.pets = pets
    this.petID = petID
  }
  
  setPets = (newPets: any) => {
    this.pets = [...newPets ]
  }

  setPetID = (newId: number) => {
    this.petID = newId
  }
}

export { PetStore }