import React from 'react'
import { autorun } from 'mobx'
// import { TitleComp } from './components/TitleCompObs.js'
import { Todos } from './components/TodosLocalStorage'

import classNames from 'classnames'
const {
  Button,
  // ButtonMarkup,
  // Link,
  View,
  Text,
  Title,
  // Rows,
  // RowItem,
  // Columns,
  // ColumnsItem,
  Section,
  Stepper,
  StepperStep,
  StepperStepMarkup,
  TitleLevel,
  // TitleMarkup,
  VariantState,
  styles
} = await import('flex_design_system_react_ts_styled')

import './styles/poker-app.css'
import 'flex-tailwind/styles/globals.css'

const App = () => {
  const defaultText = "Some quick example text to build on the card title and make up the bulk of the card's content."
  return (
    <View>
      <Section className='grid gap-4 grid-rows-1 grid-cols-1'>
        {/* <TitleComp /> */}
        <Todos />
      </Section>
      <Section className='grid gap-4 grid-rows-1 grid-cols-3'>
        {[1, 2, 3, 4].map((index) => {
          return (
            <div className={classNames(
                'card',
                'border-info'
              )}
              style={{width: '100%' }} key={index}>
              <div className='card-body'>
                <Title level={TitleLevel.LEVEL7} className={classNames('card-title' )}
                  style={{ lineHeight: 'inherit', marginBottom: '1rem' }}>
                    <p>{`Bonjour World ${index}`}</p>
                </Title>
                <p className={classNames('card-text', 'text-info')}>{defaultText}</p>
                <br/>
                <Button
                  variant={VariantState.SECONDARY}
                  className={classNames(styles.isSmall)}
                  // onClick={() => deleteTable(table.id!, table.tableId)}
                >
                    Delete this table
                </Button>
              </div>
            </div>
          )
        })}
      </Section>

      <footer className='footer'>
        <span>
          Powered by Flexiness{' '}<div className='logoBadge' />
        </span>
      </footer>
    </View>
  )
}

export { App }

// export default class App extends React.Component {
//   constructor(props: any) {
//     super(props)

//     this.state = {
//     }
//   }

//   componentDidMount() {
//     autorun(() => {
//       this.setState({
//       })
//     })
//   }

  
//   render() {
//     const defaultText = "Some quick example text to build on the card title and make up the bulk of the card's content."
//     return (
//       <View>
//         <Section className='grid gap-4 grid-rows-1 grid-cols-1'>
//           <TitleComp />
//         </Section>
//         <Section className='grid gap-4 grid-rows-1 grid-cols-3'>
//           {[1, 2, 3, 4].map((index) => {
//             return (
//               <div className={classNames(
//                   'card',
//                   'border-info'
//                 )}
//                 style={{width: '100%' }} key={index}>
//                 <div className='card-body'>
//                   <Title level={TitleLevel.LEVEL7} className={classNames('card-title' )}
//                     style={{ lineHeight: 'inherit', marginBottom: '1rem' }}>
//                       <p>{`Bonjour World ${index}`}</p>
//                   </Title>
//                   <p className={classNames('card-text', 'text-info')}>{defaultText}</p>
//                   <br/>
//                   <Button
//                     variant={VariantState.SECONDARY}
//                     className={classNames(styles.isSmall)}
//                     // onClick={() => deleteTable(table.id!, table.tableId)}
//                   >
//                       Delete this table
//                   </Button>
//                 </div>
//               </div>
//             )
//           })}
//         </Section>

//         <footer className='footer'>
//           <span>
//             Powered by Flexiness{' '}<div className='logoBadge' />
//           </span>
//         </footer>
//       </View>
//     )
//   }
// }
