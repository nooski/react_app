import React from 'react'
import { observer } from 'mobx-react-lite'
import axios from 'axios'

import classNames from 'classnames'
const {
  Button,
  // ButtonMarkup,
  // Link,
  Image,
  View,
  Text,
  Title,
  // Rows,
  // RowItem,
  // Columns,
  // ColumnsItem,
  Stepper,
  StepperStep,
  StepperStepMarkup,
  TitleLevel,
  // TitleMarkup,
  VariantState,
  styles
} = await import('flex_design_system_react_ts_styled')

const TitleComp = observer(() => {
  const [count, setCount] = React.useState(0)
  const [pets, setPets] = React.useState([])

  React.useEffect(() => {
    try {
      (async () => {
        
        if (count < 1) return

        // const { data, status } = await axios.get(`http://pets-v2.dev-apis.com/pets?id=${count}`)
        // if (data.pets) {
        //   setPets(pets => [...pets, data.pets[0] ])
        //   console.log(pets)
        // }

        const response  = await fetch(`http://pets-v2.dev-apis.com/pets?id=${count}`)
        if (response) {
          const data = await response.json()
          setPets(pets => [...pets, data.pets[0] ])
        }
        
        console.log('count', count)
        console.log('petsState', pets)
      })()
    } catch(e) {
      throw new Error(e)
    }
  }, [count])

  const handleClick = (e) => {
    setCount(count + 1)
  }

  const petMarkup = () => {
    if (!count) return
    return (
      <div className={classNames(styles.isFullwidth, styles.isFlex, styles.isJustifiedCenter)}>has pets</div>
      // <Image src={pets?.[count]?.images[0]} />
    )
  }

  return (
    <div className={classNames(styles.isFullwidth, styles.isFlex, styles.isFlexDirectionColumn, styles.isJustifiedCenter )}>
      <Button
        variant={VariantState.PRIMARY}
        onClick={(e) => handleClick(e)}>
          {`Count on me !! ${count}`}
      </Button>
      {petMarkup()}
    </div>
  )
})

export { TitleComp }