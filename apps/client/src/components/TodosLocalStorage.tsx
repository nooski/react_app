import React, { useState } from 'react'

type Todo = { text: string; done: boolean }

export const Todos = () => {
  const getInitialTodos = () => {
    const _todos = localStorage.getItem('todos');
    if (typeof _todos === 'string') return JSON.parse(_todos);
    return null;
  };
  const [text, setText] = useState('');
  const [todos, setTodos] = useState<Todo[]>(getInitialTodos ?? []);

  React.useEffect(() => {
    localStorage.setItem(`todos`, JSON.stringify(todos));
  }, [todos]);

  return (
    <div
      style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}
    >
      <h1>Todos:</h1>
      <ul>
        {todos.map((todo, index) => (
          <li key={index} style={{ margin: '8px' }}>
            <input
              type="checkbox"
              checked={todo.done}
              onChange={() => setTodos(todos.map((t, i) =>
                i === index ? { text: t.text, done: !t.done } : t
              ))}
            />
            <span
              style={{
                textDecoration: todo.done ? 'line-through' : undefined,
                margin: '16px',
              }}
            >
              {todo.text}
            </span>
            <button
              onClick={() => setTodos(todos.filter((_, i) => i !== index))}
            >
              x
            </button>
          </li>
        ))}
      </ul>
      <input
        placeholder="Type something..."
        value={text}
        onChange={(e) => setText(e.target.value)}
        onKeyDown={(e) => {
          if (e.key !== 'Enter' || !text) return;
          setTodos(todos.concat({ text, done: false }));
          setText('');
        }}
      />
    </div>
  );
};
